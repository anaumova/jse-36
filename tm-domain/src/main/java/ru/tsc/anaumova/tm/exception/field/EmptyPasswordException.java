package ru.tsc.anaumova.tm.exception.field;

import ru.tsc.anaumova.tm.exception.AbstractException;

public final class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}