package ru.tsc.anaumova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.anaumova.tm.api.repository.IProjectRepository;
import ru.tsc.anaumova.tm.model.Project;

import java.util.List;
import java.util.UUID;

public class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static long INITIAL_SIZE;

    @Before
    public void init() {
        projectRepository.create(USER_ID_1, "test-1");
        projectRepository.create(USER_ID_1, "test-2");
        projectRepository.create(USER_ID_2, "test-3");
        INITIAL_SIZE = projectRepository.getSize();
    }

    @Test
    public void create() {
        projectRepository.create(USER_ID_1, "test");
        Assert.assertEquals(INITIAL_SIZE + 1, projectRepository.getSize());
        projectRepository.create(USER_ID_1, "test", "description");
        Assert.assertEquals(INITIAL_SIZE + 2, projectRepository.getSize());
    }

    @Test
    public void add() {
        @NotNull final Project project = new Project();
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(INITIAL_SIZE + 1, projectRepository.getSize());
        Assert.assertTrue(projectRepository.findAll().contains(project));
    }

    @Test
    public void clear() {
        projectRepository.clear();
        Assert.assertEquals(0, projectRepository.getSize());
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projectsAll = projectRepository.findAll();
        Assert.assertEquals(INITIAL_SIZE, projectsAll.size());
        @NotNull final List<Project> projectsOwnedUser1 = projectRepository.findAll(USER_ID_1);
        Assert.assertEquals(2, projectsOwnedUser1.size());
        @NotNull final List<Project> projectsOwnedUser3 = projectRepository.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(0, projectsOwnedUser3.size());
    }

    @Test
    public void findOneById() {
        @NotNull final String projectName = "test find by id";
        @NotNull final Project project = projectRepository.create(USER_ID_1, projectName);
        @NotNull final String projectId = project.getId();
        Assert.assertNotNull(projectRepository.findOneById(projectId));
        Assert.assertEquals(projectName, projectRepository.findOneById(projectId).getName());
        Assert.assertNull(projectRepository.findOneById(UUID.randomUUID().toString()));
        Assert.assertNotNull(projectRepository.findOneById(USER_ID_1, projectId));
        Assert.assertEquals(projectName, projectRepository.findOneById(USER_ID_1, projectId).getName());
        Assert.assertNull(projectRepository.findOneById(USER_ID_1, UUID.randomUUID().toString()));
    }

    @Test
    public void findOneByIndex() {
        @NotNull final String projectName = "test find by index";
        projectRepository.create(USER_ID_1, projectName);
        @NotNull final Integer projectIndex = 2;
        Assert.assertNotNull(projectRepository.findOneByIndex(USER_ID_1, projectIndex));
        Assert.assertEquals(projectName, projectRepository.findOneByIndex(USER_ID_1, projectIndex).getName());
    }

    @Test
    public void existsById() {
        @NotNull final String projectName = "test exist by id";
        @NotNull final Project project = projectRepository.create(USER_ID_1, projectName);
        @NotNull final String projectId = project.getId();
        Assert.assertTrue(projectRepository.existsById(projectId));
        Assert.assertFalse(projectRepository.existsById(UUID.randomUUID().toString()));
    }

    @Test
    public void remove() {
        @NotNull final Project project = projectRepository.create(USER_ID_1, "test");
        @NotNull final String projectId = project.getId();
        projectRepository.remove(project);
        Assert.assertNull(projectRepository.findOneById(projectId));
        Assert.assertEquals(INITIAL_SIZE, projectRepository.getSize());
        projectRepository.add(project);
        projectRepository.remove(USER_ID_1, project);
        Assert.assertNull(projectRepository.findOneById(USER_ID_1, projectId));
        Assert.assertEquals(INITIAL_SIZE, projectRepository.getSize());
    }

    @Test
    public void removeById() {
        @NotNull final Project project = projectRepository.create(USER_ID_1, "test");
        @NotNull final String projectId = project.getId();
        projectRepository.removeById(projectId);
        Assert.assertNull(projectRepository.findOneById(projectId));
        Assert.assertEquals(INITIAL_SIZE, projectRepository.getSize());
        projectRepository.add(project);
        projectRepository.removeById(USER_ID_1, projectId);
        Assert.assertNull(projectRepository.findOneById(USER_ID_1, projectId));
        Assert.assertEquals(INITIAL_SIZE, projectRepository.getSize());
    }

    @Test
    public void removeByIndex() {
        projectRepository.create(USER_ID_1, "test");
        @NotNull final Integer projectIndex = 2;
        projectRepository.removeByIndex(USER_ID_1, projectIndex);
        Assert.assertThrows(IndexOutOfBoundsException.class,
                () -> projectRepository.findOneByIndex(USER_ID_1, projectIndex));
        Assert.assertEquals(INITIAL_SIZE, projectRepository.getSize());
    }

}